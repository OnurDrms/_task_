const express = require('express');
var bodyParser = require('body-parser');
var amqp = require('amqplib/callback_api');
var redis = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var redisClient= redis.createClient({
    port      : 6379,
    host      : 'redis'
});
require('./db');
var User = require('./models/users');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(session({
    secret: 'mysecret',
    store: new redisStore({client: redisClient,ttl : 260}),
    saveUninitialized: false,
    resave: false
}));

redisClient.on('error', function(err) {
    console.log('Redis error: ' + err);
});

redisClient.on("ready",function () {
    console.log("Redis is ready");
});

const router = express.Router();


const port = 8080;


router.post('/login',function(req,res){
    
    amqp.connect('amqp://rabbitmq', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
            throw error1;
            }
            var queue = 'login';
            var msg = req.body;

            channel.assertQueue(queue, {
            durable: false
            });

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
            
            channel.consume(queue, function(msg) {
                if (redisClient.connected) {
                    //Redis Search
                    redisClient.keys('User*', function (err, keys) {
                        if (err) return console.log(err);
             
                        if (keys.length > 0) {
                            for (var i = 0, len = keys.length; i < len; i++) {
                                redisClient.get(keys[i], function (err, user) {                        
                                    if (JSON.parse(msg.content.toString()).username === JSON.parse(user).username) {
                                        req.session.userId = JSON.parse(user)._id;
                                        res.json({status:"Login successful"});
                                    }else{
                                        //There is no record on Redis.
                                        var counter = 0;
                                        User.find({},function (err, doc) {
                                            if(doc.length > 0){
                                                doc.forEach(function (item) {
                                                    counter += 1;
                                                    //Write All Data To Redis
                                                    var userName = 'User' + counter;
                                                    redisClient.get(userName, function (err, user) {
                                                        if (user == null) {
                                                            var data = JSON.stringify(item._doc);
                                                            
                                                            redisClient.set(userName, data, function (err, res) { });
                                                        }
                                                    });
                                                    //End Redis Write
                                                    req.session.userId = item._id;
                                                });                                                
                                                res.json({status:"Login successful"});
                                            }else{
                                                res.json({status:"Login failed"});
                                            }
                                        });
                                    }
                                });
                            }
                        }
                        else {
                            //Read All Data From MongoDB. There is no record on Redis.
                            var counter = 0;
                            User.find({},function (err, doc) {
                                if(doc.length > 0){
                                    doc.forEach(function (item) {
                                        counter += 1;
                                        //Write All Data To Redis
                                        var userName = 'User' + counter;
                                        redisClient.get(userName, function (err, user) {
                                            if (user == null) {
                                                var data = JSON.stringify(item._doc);
                                                
                                                redisClient.set(userName, data, function (err, res) { });
                                            }
                                        });
                                        //End Redis Write
                                        req.session.userId = item._id;
                                    });
                                    res.json({status:"Login successful"});
                                }else{
                                    res.json({status:"Login failed"});
                                }
                            })
                        }
                    });  
                }
                else //If there is no connection to Redis, get all data from MongoDB.
                {      
                    User.findOne({username:JSON.parse(msg.content.toString()).username,password:JSON.parse(msg.content.toString()).password},function (err, doc) {
                        //res.send(doc);
                        if(doc){
                            req.session.userId = JSON.parse(doc)._id;
                            res.json({status:"Login successful"});
                        }else{
                            res.json({status:"Login failed"});
                        }
                    })
                }
            }, {
                noAck: true
            });
        });
    });
});
router.get('/logout', function (req, res, next) {
	if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
    	if (err) {
    		return next(err);
    	} else {
    		return res.json({status:'Logout'});
    	}
    });
}
});
router.post('/register', function(req,res){
    
    amqp.connect('amqp://rabbitmq', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
            throw error1;
            }
            var queue = 'register';
            var msg = req.body;

            channel.assertQueue(queue, {
            durable: false
            });

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
            
            channel.consume(queue, function(msg) {
                User.create({username:JSON.parse(msg.content.toString()).username,password:JSON.parse(msg.content.toString()).password},function (err, doc) {
                    if (err) return err;
                    res.json({status:"Registration Successful"});
                })              
            }, {
                noAck: true
            });
        });
    });
});

    router.post('/addProduct', function(req,res){

        amqp.connect('amqp://rabbitmq', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
            throw error1;
            }
            var queue = 'addProduct';
            var msg = req.body;

            channel.assertQueue(queue, {
            durable: false
            });

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
            
            channel.consume(queue, function(msg) {
                if(req.session.userId){
                    User.findOne({_id:req.session.userId},function(err,doc){
                        User.findOneAndUpdate({_id:req.session.userId},{products:[...doc.products,{name:JSON.parse(msg.content.toString()).name,price:JSON.parse(msg.content.toString()).price}]},{useFindAndModify: false},function(err,data){
                            if(err){
                                res.json({status:'Login first'});
                            }
                            if(data){
                                res.json({status:'Added to cart'});
                            }
                        });
                    })   
                }else{
                    res.json({status:'Login first'});
                }
            }, {
                noAck: true
            });
        });
    });
});

router.get('/products', function(req,res){
    if(req.session.userId){
        User.findOne({_id:req.session.userId},function(err,data){
            if(!data.products){
                res.json({status:'Your cart is empty'});
            }else{
                res.json({products: data.products});
            }
        });
    }else{
        res.json({status:'Login first'});
    }
});
app.use('/', router);


app.listen(port, function () {
  console.log('Example app listening on port 8080!')
})